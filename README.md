Author: Gordon Mikkelsen

Assignment #2 - Programming with
predicates

have the prolog application up and running.

the following queries are  

is there a female with the name hannah?

input
?- female(hannah).

output
true.

what is the start date for history class?

input
?- startDate(his, Y).

output
Y = '20-03-2020'.

who are the students in room 100?

input
?- studentRoom(X, 100).

output
X = jack ;
X = joe.
