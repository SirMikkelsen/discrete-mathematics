female(hannah).

male(jack).
male(donnie).
male(joe).

classes(his).
classes(eng).
classes(phy).

student(jack, his).
student(donnie, eng).
student(hannah, phy).
student(joe, his).

rooms(100, his).
rooms(101, eng).
rooms(102, phy).

startDate(his, '20-03-2020').
startDate(eng, '22-03-2020').
startDate(phy, '25-03-2020').

studentRoom(X, Y) :-
  rooms(Y, Z),
  student(X, Z).